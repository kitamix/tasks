﻿using AutoMapper;
using ProjectStructure.BL.Services.Abstract;
using ProjectStructure.Shared.DTOs;
using ProjectStructure.DAL;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.BL.Exceptions;
using ProjectStructure.DAL.Models;

namespace ProjectStructure.BL.Services
{
    public sealed class UserService : BaseService<User>
    {
        public UserService(ProjectStructureDbContext context, IMapper mapper) : base(context, mapper) { }

        public async Task<UserDTO> Create(UserDTO userDTO)
        {
            var user = Mapper.Map<User>(userDTO);
            Context.Users.Add(user);
            await Context.SaveChangesAsync();
            return Mapper.Map<UserDTO>(user);
        }
        public async System.Threading.Tasks.Task Delete(int id)
        {
            var user = await Context.Users.FindAsync(id);
            if (user == null)
            {
                throw new NotFoundException(nameof(User), id);
            }
            Context.Users.Remove(user);
            await Context.SaveChangesAsync();
        }

        public async Task<ICollection<UserDTO>> Get()
        {
            var users = await Context.Users.ToListAsync();
            return Mapper.Map<ICollection<UserDTO>>(users);
        }

        public async Task<UserDTO> Get(int id)
        {
            var user = await Context.Users.FindAsync(id);
            if (user == null)
            {
                throw new NotFoundException(nameof(User), id);
            }
            return Mapper.Map<UserDTO>(user);
        }

        public async System.Threading.Tasks.Task Update(UserDTO userDTO)
        {
            var user = await Context.Users.FindAsync(userDTO.Id);
            if (user == null)
            {
                throw new NotFoundException(nameof(User), userDTO.Id);
            }
            user.BirthDay = userDTO.BirthDay;
            user.Email = userDTO.Email;
            user.FirstName = userDTO.FirstName;
            user.LastName = userDTO.LastName;
            user.RegisteredAt = userDTO.RegisteredAt;
            user.TeamId = userDTO.TeamId;
            Context.Users.Update(user);
            await Context.SaveChangesAsync();
        }
    }
}