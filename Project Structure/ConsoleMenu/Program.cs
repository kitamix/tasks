﻿using System.Threading.Tasks;

namespace ProjectStructure.ConsoleMenu
{
    internal class Program
    {
        static async Task Main(string[] args)
        {
            while (true)
            {
               await Menu.Start();
            }
        }
    }
}