﻿using ProjectStructure.Shared.HttpServices;
using System;
using System.Threading.Tasks;

namespace ProjectStructure.ConsoleMenu
{
    public static class TaskMarker
    {
        private static readonly TasksHttpService TasksService = new();
        public static async Task<int> MarkRandomTaskWithDelay(int delay)
        {
            var tcs = new TaskCompletionSource<int>();
            var timer = new System.Timers.Timer(delay);
            timer.Elapsed += async (o, e) =>
            {
                try
                {
                    var tasks = await TasksService.GetAllUncompletedTasks();
                    if (tasks.Count == 0)
                    {
                        Console.WriteLine("All tasks marked as finished");
                        tcs.SetException(new IndexOutOfRangeException());
                    }
                    var taskIndex = new Random().Next(1, tasks.Count);
                    var taskToUpdate = tasks[taskIndex];
                    taskToUpdate.State = Shared.DTOs.TaskStateDTO.Third;
                    await TasksService.UpdateTask(taskToUpdate);
                    tcs.SetResult(taskToUpdate.Id);
                }
                catch (Exception ex)
                {
                    tcs.SetException(ex);
                }
            };
            timer.AutoReset = false;
            timer.Enabled = true;

            return await tcs.Task;
        }
    }
}