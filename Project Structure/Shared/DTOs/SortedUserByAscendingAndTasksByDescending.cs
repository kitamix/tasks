﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructure.Shared.DTOs
{
    public class SortedUserByAscendingAndTasksByDescending
    {
        public string UserName { get; set; }
        public List<TaskDTO> SortedTasks { get; set; }
    }
}