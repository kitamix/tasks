﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructure.Shared.DTOs
{
    public class UsersOlderThanTenYearsOldDTO
    {
        public int Id { get; set; }
        public string TeamName { get; set; }
        public IEnumerable<UserDTO> UsersOlderThanTenYearsOld { get; set; }
    }
}