﻿using Microsoft.AspNetCore.Mvc;
using ProjectStructure.Shared.DTOs;
using ProjectStructure.BL.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly UserService _userService;

        public UsersController(
            UserService userService
        )
        {
            _userService = userService;
        }

        [HttpPost]
        public async Task<UserDTO> Create([FromBody] UserDTO userDTO)
        {
            return await _userService.Create(userDTO);
        }
        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
            await _userService.Delete(id);
        }
        [HttpGet]
        public async Task<ICollection<UserDTO>> Get()
        {
            return await _userService.Get();
        }
        [HttpGet("{id}")]
        public async Task<UserDTO> Get(int id)
        {
            return await _userService.Get(id);
        }

        [HttpPut("{id}")]
        public async Task Update([FromBody] UserDTO userDTO)
        {
            await _userService.Update(userDTO);
        }
    }
}
